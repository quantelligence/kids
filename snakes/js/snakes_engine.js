function sleep(ms) {
  var start = new Date().getTime(), expire = start + ms;
  while (new Date().getTime() < expire) { }
  return;
}

var Random = function(choices) {
	return choices[ Math.round( Math.random() * choices.length ) ]
}

var Game = Class.extend( 
{
	init : function(player_names)
	{
		var self = this
        self.ladders = { 4 : +4, 7 : -2, 11 : +1 }
        self.players = player_names.length
        self.player_positions = [1,1,1] // [ 1 for player in player_names ] fixme
        self.current_player = 0
        self.player_names = player_names
        return self
    },
    
    current_player_name : function() 
    {
    	var self = this
        return self.player_names[ self.current_player ]
    },
    
    play_turn : function()
    {
    	var self = this
        // throw the dice
        diceValue = Random([1,2,3,4,5,6])
        console.log( self.current_player_name(), ' throws ', diceValue )
        // move the player
        self.move_player(diceValue)
        // check for ladders
        self.check_ladder()
        // if player has won then it stops otherwise, go to next player
        if ( self.player_has_won() )
            console.log( self.current_player_name(), ' has won!!!' )
        else
            self.next_player()
    },
    
    check_ladder : function()
    {
    	var self = this;
        position = self.current_player_position() 
        if ( position in self.ladders )
        {
            step = self.ladders[ position ]
            console.log( 'Found ladder/snake: ', step )
            self.move_player(step)
        }
   	},
   	
   	finished : function() { return this.player_has_won() },
   	current_player_position : function() 
   	{ 	
        var self = this;
        return self.player_positions[ self.current_player ]
    },
    
    player_has_won : function() { 
        var self = this;
        var BOARD_SIZE = 15;
        return self.current_player_position() >= BOARD_SIZE
    },
    
    next_player : function()
    {
    	var self = this;
        self.current_player = self.current_player + 1
        if ( self.current_player == self.players )
            self.current_player = 0
    },
    
    move_player : function(diceValue) 
    {
    	var self = this;
        console.log('Moving ',  self.current_player_name(), ' by ', diceValue )
        self.player_positions[ self.current_player ] = self.player_positions[ self.current_player ] + diceValue  // [1,1,1] [0] -> 1
    },

	print : function()
	{
    	var self = this
        console.log( '>> Game: current player=',self.current_player_name(), ' positions=', self.player_positions )
    }


} ) // Class.extend


