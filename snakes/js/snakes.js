backgroundParams = { 
	x : 30,
	y : 30,
	width : 556,
	height : 500 }

function CellCenter(cellnumber)
{
	x = (cellnumber-1) % 10 + 1
	y = Math.floor((cellnumber-1)/10)
	if ( ( y % 2 ) )
		x = 11 - x
		
	bp = backgroundParams
	
	position_x = bp.x + bp.width / 10 * (x-1)
	position_y = bp.y + bp.height * ( 1 - 1 / 10 * (y+1) )

	var imageObj = new Image();

	var image = new Kinetic.Image( {
	  x : position_x,
	  y : position_y,
	  width : bp.width/10,
	  height : bp.height/10,
	  image: imageObj,
	} );
	
	imageObj.src = 'images/Saddle_Brook_Historical_Society.jpg';
	
	return image
}

function Draw(elements)
{
    elements.map( function(item) { shapesLayer.add(item) } );
    stage.add(shapesLayer);
}

function Initialize()
{
    stage 		 = new Kinetic.Stage(
                            {
                               container	 : 'container',
                               width		 : parameters.CANVAS_WIDTH,
                               height		 : parameters.CANVAS_HEIGHT
                            }
                    );
    shapesLayer = new Kinetic.Layer();	
}		


function InitializeVariables()
{
	boxposition     = parameters.BOXPOSITION_START;
	texttodisplay = 'Hello'
	inpause			= false
	score			= 0 
	level			= 1
	totalmissed		= 0
	message			= "You're doing great - having fun?"
	autopilot		= false
	cell			= 1
}

function CleanUp()
{
    stage.clear();
    stage.removeChildren();
    shapesLayer.removeChildren();
}


function StartGameLoop()
{
    var interval;
    $(window).blur(function(e) {
        if( running )
            clearInterval(interval);
        running = false;
    });
    $(window).focus(function(e) {
        if ( !running)
           interval = setInterval(GameLoop, parameters.GAME_LOOPINTERVAL); 
       running = true;
    });
    
    running = true;
    interval = setInterval(GameLoop, parameters.GAME_LOOPINTERVAL); 
}


function Border()
{
    params = { x : 0, y : 0, width : 900, height : 550,  strokeWidth : 1,
                    fillLinearGradientStartPoint: [0, 0],
          fillLinearGradientEndPoint: [1500, 1500],
          fillLinearGradientColorStops: [0, 'white', 1, 'yellow']
            }; 

    return new Kinetic.Rect(params);
}

function Background()
{
	var imageObj = new Image();
	
	var image = new Kinetic.Image( _.extend( backgroundParams, {
	  image: imageObj,
	}) );
	
	imageObj.src = 'images/chutesladders.jpg';

	return image
}

function Side()
{
    params = { x : 700, y : 0, width : 300, height : 550,  strokeWidth : 1,
                    fillLinearGradientStartPoint: [0, 0],
          fillLinearGradientEndPoint: [1500, 1500],
          fillLinearGradientColorStops: [0, 'white', 1, 'yellow']
            }; 

    return new Kinetic.Rect(params);
}

function GameLoop()
{
    CleanUp();
    
	texttodisplay = ""
	texttodisplay += "Score: " + score + "\n"
	texttodisplay += "Level: " + level  + "\n"
	texttodisplay += "Total missed: " + totalmissed + " out of 3\n"
	texttodisplay += "\n\n"	
	texttodisplay += autopilot ? "[autopilot]\n" : "\n"
	texttodisplay += message
	
    text   = PrintText(texttodisplay)
    Draw( [ Border(), Side(), Background(), CellCenter(cell), text ] )
/*    cell++
	if ( cell > 100 )
		cell = 1
*/
}

function Move(side)
{
    var move = parameters.BASKET_MOVE_SPEED;
    if ( side === "left" )
            boxposition = boxposition - move;
    if ( side === "right" )
            boxposition = boxposition + move;

    if ( boxposition > 600 )
            boxposition = 0;
    if ( boxposition < 0 )
            boxposition = 600;
}

function PrintText(text)
{
    return new Kinetic.Text( {
                                    x	    	: 720, y : 50, text : text, fill : 'black', fontSize : 20,
			})
}


parameters = { 
                'GAME_LOOPINTERVAL'     :   100,
                'CANVAS_WIDTH'          :   1100,
                'CANVAS_HEIGHT'         :   550,
             };

function StartGame()
{
	Initialize();
	InitializeVariables();

	StartGameLoop();
	//gameLoop()
}


