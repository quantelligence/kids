window.FC = window.FC || {};

FC.GUI = Class.extend( {
	init : function(canvasid,tick) 
	{
		var self = this;
		this.stage = new Stage(document.getElementById(canvasid));
		this.stage.tick = function() { tick() };
		this.viewables = {}
	},
	
	add : function(name,viewable,layername,visible) {
		var layer = this.getlayer(layername);
		visible == undefined ? visible = true : null;
		this.viewables[name] = { layer : layername, viewable : viewable };
		if ( visible )
			viewable.addto(layer);
	},
	
	getlayer : function(name) {
		return ( name == undefined ) ? this.stage : this.v(name).view;	
	},
	
	remove : function(name) {
		var layername = this.viewables[name].layer;
		var layer = this.getlayer(layername);
		this.v(name).removefrom(layer);
	},
	
	v : function(name) { return this.viewables[name].viewable },
	
	update : function() { this.stage.update() },
	

} );

// Generic parent class for objects that have the ability to generate
// some bitmap (stored in view attribute)
Viewable = Class.extend( {
	init		: function(obj) { this.view = obj },
	addto 		: function(layer)  {	layer.addChild(this.view);		return this;	},
	removefrom 	: function(layer)  {	layer.removeChild(this.view);	return this;	},
	x			: function() { return this.view.x },
	y			: function() { return this.view.y },
	tween		: function(params, delay) {
		return Tween.get(this.view).to(params,delay);
	}
} );

FC.FruitPicker = Class.extend( {

	images : {
		            'fruit'     :       [ 'Apple', 'Apricot', 'Banana', 'Cherry', 'Kiwi', 'Lemon', 'Mango', 'Orange', 'Peach', 'Strawberry', 'Tomato' ],
		            'toy'       :       [ 1,2,3,4,5,6,7,8,9 ]
		     },
		         
	file_names : {      'fruit'     : 'images/fruits/<%=image_name%>128.png', 
		                'toy'       : 'images/Toys/toy0<%=image_name%>.png' 
		         },

	init : function(type,name) {
		var self = this;
		self.type = type;
		if ( type in this.file_names )
		{
			self.image_name = _.template( this.file_names[type], 
										 { image_name : name } ); 
			self.bitmap_stored = self.bitmap();
		}
	},

	bitmap : function() {
		var self = this;
		var scale = 0.5;
	   	var imageObj = new Bitmap(self.image_name);
		imageObj.scaleX = imageObj.scaleY = scale;
		return imageObj;
	},
	
	addToStage : function(stage) {
		var self = this;
		var bitmap = self.bitmap_stored
		stage.addChild(bitmap);
		return bitmap;
	},

	randomize : function()
	{
		var type_of_object = [ 'fruit', 'toy' ].randomchoice();
		type_of_object = 'fruit';
		
		var name = this.images[type_of_object].randomchoice();
		this.init(type_of_object,name);
		return this;
	},
	
	object : function(speed) {
		var fruit   	= new FC.Fruit(this.image_name);
		fruit.view.x	= Random(50,550); 
		fruit.view.y	= 0;
		fruit.type 		= this.type;
		return fruit;
	}

} );

FC.Fruit = Viewable.extend( { 

	init : function(image_file,falling_speed) {
		var self = this;
		var fruit = GetBitmap(image_file,null,null,0.5)
		fruit.regX = fruit.regY = 70
		fruit.y = 10;
		
		self.view 			= fruit;
		self.direction 		= Math.random() * 2 - 1
		self._speedup 		= 1;
		self.falling_speed 	= falling_speed;
	},

	tick : function() {
	    var self = this;
	    self.view.y 	   += self.falling_speed * self._speedup
	    self.view.rotation += self.direction *  self.falling_speed * self._speedup;
	},
	
	speedup : function() { this._speedup = 5 },
	
	islow : function(basket) {
		return ( this.y() > basket.top() ) && 	
			   ( this.y() < basket.bottom() )
	},

	lowerthan : function(basket) {
		return	( this.y() > basket.bottom() )
	},
} );


FC.Basket = Viewable.extend( { 

	init : function(x,y,width,height,scale) {
		var self = this;
		var image_file = 'images/pou.png';
		image_file = 'images/basket-xxl.png';    
		image_file = 'images/fruitbasket.gif';    
		var bitmap = GetBitmap(image_file,x,y,scale);
		bitmap.regX = bitmap.regY = 50;
		self.view = bitmap;
		self.width = width;
		self.height = height;
	},
	
	align : function(fruit) {
		this.view.x = fruit.x() - this.height / 2;
		return this;
	},
	
	contains : function(fruit) {
		return ( fruit.x() > this.x() ) 
			&& ( fruit.x() < this.x() + this.width )
	},
	
	bottom : function() { 
		return this.y() + this.height;
	},
	
	top : function() {
		return this.y();
	},
	
} );

FC.TextLabel = Viewable.extend( { 
	init : function(x,y,colour)
	{
		var text = new Text("", "bold 14px Arial", colour );
		text.x = x
		text.y = y
		this.view = text;	
		return this;
	}
} );

FC.Challenge = Viewable.extend( { 

	init : function() {
		this.a = Math.floor( Math.random() * 8 ) + 2
		this.b = Math.floor( Math.random() * 8 ) + 2
		this.values = [0,0]
		
		var view = new Text( this.text(), "bold 36px Loved by the King" );		
		view.y = view.x = 75;
		this.view = view;
	},
	
	text : function() {
		return this.a + "x" + this.b + "=" + this.value() + "?";
	},
	
	update : function() { 
		this.view.text = this.text();
	},
	
	value : function() { 
		return this.values[0]*10+this.values[1];
	},
	
	typevalue : function(value) {
		this.values[0] = this.values[1]
		this.values[1] = value;
		this.update();
	},
	
	checks : function(value) {
		return this.a * this.b == this.value();
	},
	
	alignto : function(fruit) {
		this.view.x = fruit.x() + 30;
		this.view.y = fruit.y();
	},
	
	disappear : function() {
		this.tween( {alpha:0}, 200 );
	}

} );

