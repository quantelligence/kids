// Keyboard input with customisable repeat (set to 0 for no key repeat)
//
function KeyboardController(keys, repeat) {
    // Lookup of key codes to timer ID, or null for no repeat
    //
    var timers= {};

    // When key is pressed and we don't already think it's pressed, call the
    // key action callback and set a timer to generate another one after a delay
    //
    document.onkeydown= function(event) {
        var key= (event || window.event).keyCode;
        if (!(key in keys))
            return true;
        if (!(key in timers)) {
            timers[key]= null;
            keys[key]();
            if (repeat!==0)
                timers[key]= setInterval(keys[key], repeat);
        }
        return false;
    };

    // Cancel timeout and mark key as released on keyup
    //
    document.onkeyup= function(event) {
        var key= (event || window.event).keyCode;
        if (key in timers) {
            if (timers[key]!==null)
                clearInterval(timers[key]);
            delete timers[key];
        }
    };

    // When window is unfocused we may not get key events. To prevent this
    // causing a key to 'get stuck down', cancel all held keys
    //
    window.onblur= function() {
        for (key in timers)
            if (timers[key]!==null)
                clearInterval(timers[key]);
        timers= {};
    };
};


function autorescale(stage,canvas,scalingfactor) {

		var basedimensions = [ canvas.width, canvas.height ];

		// function to rescale canvas when window is resized
		return function(ev) {
			var dimensions = [ window.innerWidth, window.innerHeight ]

			var ratios = [ dimensions[0] / basedimensions[0], 
						   dimensions[1] / basedimensions[1] ]
			var scale = Math.min(ratios[0],ratios[1]) * scalingfactor;

			stage.scaleX = stage.scaleY = scale;
			canvas.width = basedimensions[0] * scale;
			canvas.height = basedimensions[1] * scale;
		}
}

	

function Random(x,y)
{
    return Math.random() * ( y-x ) + x;
}

