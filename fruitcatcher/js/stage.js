Border = Viewable.extend( {

	init : function()
	{
		var background = new Shape();
		background.graphics.beginLinearGradientFill(["white","yellow"], [0, 1], 0, 0, 1500, 1000).drawRect(0,0,850,500)

		this.view = background;
		return this;
	}

} );

TitlePage = Viewable.extend( {

	init : function(stage) {
		stage.enableMouseOver();
	
		var page = new Container();
		page.x = 150;
		page.y = 150;
		page.scaleX = page.scaleY = 1.6;
	
		var title = new Text("Fruitcatcher", "bold 72px Loved by the King", "darkblue" )
		title.x = -200;
		title.y = -500;
		title.rotation = 150;
		title.alpha = 0;
	
		Tween.get(title)
		 .to({x:100,y:75,rotation:0,alpha:1},500)

		var button = this.button = new Text("Loading...", "bold 48px Loved by the King", "darkblue" );
		button.x = 150;
		button.y = 150;
		var hit = new Shape();
		var width = button.getMeasuredWidth();
		var height = button.getMeasuredLineHeight();
		hit.graphics.beginFill("#fff").drawRect(button.x,button.y-height,width,height)
		hit.alpha = 0.01
	
		page.addChild(hit,button,title)
	
		var fruits = [ 
			{ name : 'Apple',	x : 45,		y : 60, r : 0 },
			{ name : 'Banana',	x : 377,	y : 33, r : 115 },
			{ name : 'Kiwi',	x : 150,	y : -45, r : 0 },
			{ name : 'Mango',	x : 230,	y : 75, r : 0 },
			{ name : 'Cherry',	x : 260,	y : -45, r : 15 },
			{ name : 'Strawberry',	x : 60,	y : -45, r : -15 },
		]

		fruits.map( function(item) {
			var f = new FC.FruitPicker('fruit',item.name);
			f.bitmap_stored.x = item.x
			f.bitmap_stored.y = item.y;
			f.bitmap_stored.alpha = 0;
			f.bitmap_stored.rotation = ( Math.random() * 2 - 1 ) * 1000;
			f.addToStage(page)	
			Tween.get(f.bitmap_stored).to( { rotation: item.r, alpha: 1 }, 1500 )
		} );		

		this.view = page;
	},

	removefrom : function(stage) {	
		this.tween( {y:-500,alpha:0,rotation:50},500 )
   		    .call( function() { stage.removeChild(this); } )
	},
	
	readymode : function(onclick_cb) {
		var startbutton = this.button;
		startbutton.text = "Start";
		this.view.onClick = onclick_cb;
		this.view.onMouseOut = function(e) 
		{ 
			startbutton.scaleX = startbutton.scaleY = 1; startbutton.rotation = 0; 
		};
		this.view.onMouseOver = function(e) 
		{
			startbutton.scaleX = startbutton.scaleY = 1; startbutton.rotation = -10; 
		};
	}

} );

GameOver = Viewable.extend( {
	init : function(stage) {
		var text = new Text("Game over!", "bold 96px Loved by the King", "Black");
		text.scaleX = text.scaleY = 0.1;
		this.view = text;
		this.addto(stage)
			.tween({scaleX:1,scaleY:1,y:200,x:200},500);
	},
} )

