( function() {

	function handleFileLoad(event) {
		     //triggered when an individual file completes loading
		         
		     switch(event.type)
		     {
		        case PreloadJS.IMAGE:
		        //image loaded
		         var img = new Image();
		          img.src = event.src;
		          img.onload = handleLoadComplete;
		          window[event.id] = new Bitmap(img);
		        break;
	 
		        case PreloadJS.SOUND:
		        //sound loaded
		        handleLoadComplete();
		        break;
		     }
	}
	

	function LoadAssets(cb) {

		var	manifest = [
				        {src:"sounds/cestbon3.mp3", id:"apple", data : 4},
				        {src:"sounds/shocking.mp3", id:"error", data : 4 },
		//                {src:"sounds/22558-apple.mp3", id:"apple"},
		//                {src:"sounds/76527-error.mp3", id:"error"},
		//                {src:"sounds/1680-gameover.mp3", id:"gameover"},
				        {src:"sounds/gameover.mp3", id:"gameover", data: 4},
				        {src:"sounds/T-Rio - Choopeta.mp3", id : "music" },
				    ];


		var preloader = new PreloadJS();
		preloader.installPlugin(SoundJS);
		preloader.onComplete = cb
		preloader.loadManifest(manifest);
		return preloader;
	}
	
	window.FC = window.FC || {};
	window.FC.LoadAssets = LoadAssets
} )()
