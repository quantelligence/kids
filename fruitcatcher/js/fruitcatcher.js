Array.prototype.randomchoice = function () {
       return this[Math.floor((Math.random()*this.length))];
 };



function RegisterKeyStrokes()
{
	var cycle = parameters.GAME_LOOPINTERVAL
	KeyboardController({
		37: function() { Move("left"); },
		39: function() { Move("right") },
		65: function() { autopilot = !autopilot },
		80: function() { inpause   = !inpause },
	}, cycle);
}

// APPLICATION

App = Class.extend( { 

	init : function(canvasid,parameters)
	{
		var self = this;
		self.parameters = parameters;
		var tick_fn = function() { self.tick() };
		var gui = this.gui  = new FC.GUI(canvasid,tick_fn); 

		// add border
		gui.add( "border", new Border() );
		window.onresize = autorescale(gui.stage,canvas,1.1);
		window.onresize();
	
		// add titlepage
		gui.add( "titlepage", new TitlePage(gui.stage) );
		inpause = true;

		// boilerplate code to link ticker to gui.stage	
		Ticker.addListener(gui.stage);
		Ticker.setFPS(30);

		// loadassets and when ready turn 'loading button' into 'start' button	
		var startgame = function() { self.StartGame() }; 
	 	var oncomplete = function(ev) { gui.v("titlepage").readymode(startgame) };
		FC.LoadAssets(oncomplete);
	},
	
	newgame : function()
	{
		var self 		= this;
		inpause			= true
		autopilot		= false
		speed			= 0;
		
		this.game		= new Game( self.parameters.TOTAL_LIVES );
		
		var fruit  			= new Viewable();
		this.gui.add('fruit',fruit);
		this.gui.add( 'challenge', new FC.Challenge(), null, false );
	},

	StartGame : function()
	{
		var self = this;
		var parameters = self.parameters;
		var gui = this.gui;
		
		self.setkeydownhandler();
		
		if ( self.parameters.PLAY_MUSIC )
			SoundJS.play("music",null,null,null,null,0.1);

		gui.remove('titlepage');
	
		var box    = new FC.Basket( parameters.BASKET_X, parameters.BASKET_Y, 
								 parameters.BOX_POSITION_WIDTH, parameters.BASKET_HEIGHT, 
								 parameters.BASKET_SCALE );
		gui.add('fruitlayer', new Viewable( new Container() ) );
		this.GetNewObject();

		gui.add('box',box);

		text = new FC.TextLabel(parameters.TEXT_X, parameters.TEXT_Y, 						parameters.TEXT_COLOUR).addto(app.gui.stage);
	//	RegisterKeyStrokes();
	
		inpause = false;
	},

	GameLoop : function()
	{   
		var gui = this.gui;
		var game = this.game;
		var parameters = this.parameters;
	
		var box = gui.v('box');
		var fruit = gui.v('fruit');
		game.process( box, fruit );
	
		var texttodisplay = game.text();

		if ( game.caught )
		{
			if ( fruit.type == 'fruit' )
			{
				SoundJS.play("apple");
				game.markpoint();
				parameters.OBJECT_FALLING_SPEED *= 1.02
			}
			else
			{
				SoundJS.play("error");
				game.miss();
			}	
		
			this.GetNewObject()		
		}
	
		if ( game.missed )
		{
			if ( fruit.type == 'fruit' )
			{
				SoundJS.play("error");
				game.miss();
			}
			else
				parameters.OBJECT_FALLING_SPEED *= 1.02							
			
			this.GetNewObject()		
		}

		if ( game.finished() )
		{
			message = "YOU FAILED!!!"
			if ( !inpause )
				SoundJS.play("gameover");
			var gameover = new GameOver(gui.stage);
			inpause = true
		}

		if ( autopilot )	// old code
		{
			target = fruit.x - parameters.BOX_POSITION_WIDTH / 2

			var diff = 0.2 * ( target - box.x )
			var speed = parameters.BASKET_MOVE_SPEED
	//		diff = Math.max( -speed, Math.min(diff, speed) )
			box.x += diff
		}

		text.view.text = texttodisplay
	
		if ( !inpause )
		{
			fruit.tick();
			var challenge = gui.v('challenge');
			challenge.alignto(fruit)
		}
	
		speed -= 0.5
		speed = Math.max(0,speed);
	},
	
	GetNewObject : function() 
	{
		var self = this;
		var game = self.game;
		var gui  = self.gui;
	
		game.newobject();
		gui.remove('fruit','fruitlayer');
		var fruit           = new FC.FruitPicker().randomize().object();
		fruit.falling_speed	= self.parameters.OBJECT_FALLING_SPEED;
		gui.add('fruit',fruit,'fruitlayer');
	
		gui.remove('challenge');//.removefrom( gui.stage )
		var challenge = new FC.Challenge();
		gui.add('challenge',challenge);
	},
	
	setkeydownhandler : function() {
		var self = this;
		
		document.onkeydown = function(e) {
			if ( e.keyCode >= 96 && e.keyCode <= 105 )
			{
				value = e.keyCode - 96
				var gui = self.gui
				var challenge = gui.v("challenge");
				challenge.typevalue(value);
				if ( challenge.checks() )
				{
					var box = gui.v('box');
					var fruit = gui.v('fruit');
					box.align(fruit);
					fruit.speedup()
					challenge.disappear();
				}
			}
		}
	},
	
	tick : function()
	{
		if (!inpause)
			this.GameLoop();
		
		this.gui.stage.update();
	}


	
} );	// App class


	

function GetBitmap(image_file,x,y,scale)
{
   	imageObj = new Bitmap(image_file);
	imageObj.x = x
	imageObj.y = y;
	imageObj.scaleX = imageObj.scaleY = scale;
		
	return imageObj	
};


Game = Class.extend( { 
	init : function(totallives) {
		var self = this;
		self.score = 0;
		self.level = 1;
		self.totalmissed = 0;
		self.totallives = totallives;
	},
	
	text : function() {
		var self = this;
		var texttodisplay = 	"Score: " + self.score + "\n" + 
								"Level: " + self.level  + "\n" + 
								"Total missed: " + self.totalmissed + " out of " + self.totallives + "\n\n\n"
		return texttodisplay;
	},
	
	markpoint 	: function() { this.score++ },
	miss 		: function() { this.totalmissed++ },
	finished 	: function() { return this.totalmissed >= this.totallives; },
	
	newobject : function() {
		this.caught 		= false
		this.missed			= false
		return this;
	},
	
	process : function( box, fruit ) {
		var justcaught  = box.contains(fruit) && fruit.islow(box);
		this.caught		= this.caught || justcaught;
		this.missed     = !this.caught && fruit.lowerthan(box)
		return this;
	}
} );

	
function Move(side)
{
	var sign = { "left" : -1, "right" : 1 }[side];
	speed += 0.5
	speed  = Math.min(speed,parameters.BASKET_MOVE_SPEED);
	
	var move = speed;
    box.x += sign * move;

    if ( box.x > 600 )
            box.x = 0;
    if ( box.x < 0 )
            box.x = 600;
}


function StartApp(canvasid)
{
	var parameters = { 
                'BASKET_SIZE'           :   5,
                'BASKET_SCALE'			:   0.6,
                'BASKET_X'              :   200,
                'BASKET_Y'              :   450,
				'BASKET_HEIGHT'			:	40,
                'GAME_LOOPINTERVAL'     :   30,
                'OBJECT_FALLING_SPEED'  :   3,
                'BASKET_MOVE_SPEED'     :   15,
				'BOX_POSITION_WIDTH'	: 	70,
				'TEXT_X'				:	600,
				'TEXT_Y'				:	25,
				'TEXT_COLOUR'			:	'darkblue',
				'TOTAL_LIVES'			:	3,
				'PLAY_MUSIC'			:	false,
             };

	app = new App(canvasid,parameters);
	app.newgame();
}
